package socketgo

import (
	"github.com/gorilla/websocket"
	"github.com/rs/xid"
	"net/http"
	"sync"
)

var wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type SocketHandler struct {
	mutex       sync.Mutex
	connections map[string]*SocketConnection
	OnMessage   func(*SocketConnection, interface{})
}

func NewHandler(onmessage func(*SocketConnection, interface{})) SocketHandler {
	return SocketHandler{
		connections: make(map[string]*SocketConnection),
		OnMessage:   onmessage,
	}
}

// Upgrade returns a unique hash for that connection
func (h *SocketHandler) Upgrade(w http.ResponseWriter, r *http.Request) (error, string) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		return err, ""
	}

	hash := xid.New().String()

	h.mutex.Lock()
	h.connections[hash] = &SocketConnection{
		connection: conn,
		hash:       hash,
	}
	h.mutex.Unlock()

	go func() {
		h.mutex.Lock()
		conn := h.connections[hash]
		h.mutex.Unlock()

		err := conn.listen(func(msgType int, msg []byte) error {
			if h.OnMessage != nil {
				h.OnMessage(conn, msg)
			}
			return nil
		})
		if err != nil {
			h.mutex.Lock()
			_ = h.connections[hash].connection.Close()
			delete(h.connections, hash)
			h.mutex.Unlock()
		}
	}()

	return nil, hash
}

func (h *SocketHandler) Broadcast(msg []byte) error {
	h.mutex.Lock()
	for _, conn := range h.connections {
		err := conn.Send(msg)
		if err != nil {
			h.mutex.Unlock()
			return err
		}
	}
	h.mutex.Unlock()

	return nil
}

func (h *SocketHandler) BroadcastJSON(msg interface{}) error {
	h.mutex.Lock()
	for _, conn := range h.connections {
		err := conn.SendJSON(msg)
		if err != nil {
			h.mutex.Unlock()
			return err
		}
	}
	h.mutex.Unlock()

	return nil
}

func (h *SocketHandler) Send(hash string, msg []byte) error {
	h.mutex.Lock()
	conn, ok := h.connections[hash]
	h.mutex.Unlock()

	if ok {
		return conn.Send(msg)
	}
	return nil
}

func (h *SocketHandler) SendJSON(hash string, msg interface{}) error {
	h.mutex.Lock()
	conn, ok := h.connections[hash]
	h.mutex.Unlock()

	if ok {
		return conn.SendJSON(msg)
	}
	return nil
}
