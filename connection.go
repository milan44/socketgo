package socketgo

import "github.com/gorilla/websocket"

type SocketConnection struct {
	connection *websocket.Conn
	hash       string
}

func (s *SocketConnection) listen(onmessage func(int, []byte) error) error {
	for {
		t, msg, err := s.connection.ReadMessage()
		if err != nil {
			return err
		}

		err = onmessage(t, msg)
		if err != nil {
			return err
		}
	}
}

func (s *SocketConnection) Send(msg []byte) error {
	return s.connection.WriteMessage(websocket.BinaryMessage, msg)
}

func (s *SocketConnection) SendJSON(msg interface{}) error {
	return s.connection.WriteJSON(msg)
}
