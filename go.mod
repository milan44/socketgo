module gitlab.com/milan44/socketgo

go 1.15

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/rs/xid v1.3.0 // indirect
)
